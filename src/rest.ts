import axios, { AxiosRequestConfig } from 'axios';


export interface ILoginPayload {
  email: string;
  password: string;
}

export interface IRegisterPayload {
  email: string;
  password: string;
}

export const AxiosConfig: AxiosRequestConfig = {
  baseURL: process.env.VUE_APP_API_URL,
};

export default class Rest {
  static userMe() {
    const url = 'user/me';
    return axios.get(url);
  }

  static login(payload: ILoginPayload) {
    const url = `auth/login`;
    return axios.post(url, payload);
  }

  static register(payload: IRegisterPayload) {
    const url = `auth/register`;
    return axios.post(url, payload);
  }

  static createRandomUser() {
    const url = `auth/create_user`;
    return axios.post(url);
  }

  static createAdminUser() {
    const url = `auth/create_admin_user`;
    return axios.post(url);
  }

  static cabinetPage(userId: any, page: number, subPage: number) {
    const url = `docs/user_docs/${userId}/${page}/${subPage}`;
    return axios.get(url);
  }

  static saveCommonPage(form: any) {
    const url = `docs/saveCommon`;
    return axios.post(url, form);
  }

  static getAdminHome() {
    const url = `user/users`;
    return axios.get(url);
  }
}
