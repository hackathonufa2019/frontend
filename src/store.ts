import Vue from 'vue';
import Vuex from 'vuex';
import authModule from '@/views/auth/store';
import cabinetModule from '@/views/cabinet/store';
import adminModule from '@/views/admin/store';

Vue.use(Vuex);


export interface IAppState {

}

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth: authModule,
    cabinet: cabinetModule,
    admin: adminModule,
  },
});
