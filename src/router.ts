import Vue from 'vue';
import Router, { Route } from 'vue-router';
import { VueRouter } from 'vue-router/types/router';
import store from '@/store';
import { AUTH_INIT } from '@/views/auth/store';

Vue.use(Router);

const router: VueRouter = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/home/home.vue'),
    },
    {
      path: '/home_admin',
      name: 'home_admin',
      component: () => import('@/views/admin_home/home.vue'),
    },
    {
      path: '/auth',
      name: 'auth',
      component: () => import('@/views/auth/auth.vue'),
    },
    {
      path: '/admin',
      name: 'admin',
      component: () => import('@/views/admin/admin.vue'),
      beforeEnter: (to, from, next) => {
        store.dispatch('admin/init')
          .then(() => {
            next();
          })
          .catch(() => {
            next();
          });
      },

    },
    {
      path: '/cabinet',
      name: 'cabinet',
      component: () => import('@/views/cabinet/cabinet.vue'),
      beforeEnter: (to, from, next) => {
        console.log(to.query);
        const page = parseInt(to.query.page as string) || 1;
        const subPage = parseInt(to.query.subPage as string) || 1;
        store.dispatch('cabinet/cabinetInit', { page, subPage })
          .then(() => {
            next();
          })
          .catch(() => {
            next();
          });
      },
    },
  ],
});
router.beforeEach((to: Route, from: Route, next: (path?: string) => void) => {


  const userMePromise = store.dispatch('auth/' + AUTH_INIT);

  Promise.all([ userMePromise ]).then(() => next(), () => next());


});


export default router;
