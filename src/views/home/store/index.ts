import { Module } from 'vuex';
import { IAppState } from '@/store';

interface IHomeState {

}

const homeModule: Module<IHomeState, IAppState> = {};

export default homeModule;
