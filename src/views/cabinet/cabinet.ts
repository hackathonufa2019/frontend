import { Component, Vue } from 'vue-property-decorator';
import SideBar from './components/sidebar/sidebar';
import { namespace } from 'vuex-class';
import CommonInfo from '@/views/cabinet/components/common_info/common_info';
import RatePlan from '@/views/cabinet/components/rate-plan/rate-plan';
import { saveInfo } from '@/views/cabinet/store';
import RequestHistory from '@/views/cabinet/components/request_history/request_history';

const cabinetNs = namespace('cabinet');
@Component({
  components: {
    SideBar,
    CommonInfo,
    RatePlan,
    RequestHistory,
  },
})
export default class Cabinet extends Vue {
  @cabinetNs.Action(saveInfo) onSave: any;
  @cabinetNs.State(state => state.page) page: number;
  @cabinetNs.State(state => state.subPage) subPage: number;
  @cabinetNs.State(state => state.pageData) pageData: any;

  get pageComponent(): any {
    const cmpMap: any = {
      1: {
        1: CommonInfo,
        3: RatePlan,
      },
      4: {
        1: RequestHistory,
      },
    };
    return (cmpMap[ this.page ] || {})[ this.subPage ];
  }
}
