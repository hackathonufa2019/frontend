import { Component, Prop, Vue } from 'vue-property-decorator';


@Component
export default class SideBar extends Vue {
  @Prop() selectedMainItem: number;
  @Prop() selectedSubItem: number;

  items: any[] = [
    {
      title: 'Ваша заявка',
      id: 1,
      expanded: true,
      items: [
        {
          title: 'Информация о заявителе',
          id: 1,
        },
        {
          title: 'Подготовленные документы для печати',
          id: 2,
        },
        {
          title: 'План расходов гранта на поддержку начинающего фермера',
          id: 3,
        },
        {
          title: 'Информация о крестьянском (фермерском) хозяйстве',
          id: 4,
        },
      ],
    },
    {
      title: 'Статус заявки',
      id: 2,
      expanded: false,
    },
    {
      title: 'Результаты комиссии',
      id: 3,
      expanded: false,
    },
    {
      title: 'Моя история заявок',
      id: 4,
      expanded: false,
    },
  ];

  goTo(page:number, subPage:number=1) {
    window.location.href = `/cabinet?page=${page}&subPage=${subPage}`
  }
}
