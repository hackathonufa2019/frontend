import { Component, Prop, Vue, Watch } from 'vue-property-decorator';

@Component
export default class RatePlan extends Vue {
  @Prop({ default: (): any => null }) pageData: any;

  form: any = null;

  @Watch('pageData')
  defS(val: any) {
    this.form = { ...val };
  }

  save() {
    this.$emit('on-save', this.form);
  }


  cols: any[] = [
    {
      typeId: 1,
      first: '',
      qty: 0,
      pricePerOne: null,
      fromGrant: 0,
    },
    {
      typeId: 2,
      first: '',
      qty: 0,
      pricePerOne: null,
      fromGrant: 0,
    },
    {
      typeId: 3,
      first: '',
      qty: 0,
      pricePerOne: null,
      fromGrant: 0,
    },
    {
      typeId: 4,
      first: '',
      qty: 1,
      pricePerOne: 12,
      fromGrant: 0,
    },
    {
      typeId: 5,
      first: 1,
      qty: 0,
      pricePerOne: null,
      fromGrant: 0,
    },
  ];

  getTitle(typeId: number): string {
    const typeMap: any = {
      1: 'Приобретение посадочного материала для закладки многолетних насаждений, включая виноградники, шт.',
      2: 'Приобретение сельскохозяйственной техники и инвентаря, грузового автомобильного транспорта, оборудования для производства и переработки сельскохозяйственной продукции, срок эксплуатации которых не превышает 3 лет, шт.',
      3: 'Подключение производственных и складских зданий, помещений, пристроек и сооружений, необходимых для производства, хранения и переработки сельскохозяйственной продукции, к инженерным сетям - электрическим, водо-, газо- и теплопроводным сетям, руб.',
      4: 'Разработка проектной документации для строительства (реконструкции) производственных и складских зданий, помещений, предназначенных для производства, хранения и переработки сельскохозяйственной продукции, руб.',
      5: 'Приобретение, строительство, ремонт и переустройство производственных и складских зданий, помещений, пристроек, инженерных сетей, заграждений и сооружений, необходимых для производства, хранения и переработки сельскохозяйственной продукции, а также на их регистрацию, руб.',

    };
    return typeMap[ typeId ];
  }

  sum(col: any) {
    return (col.qty || 0) * (col.pricePerOne || 0);

  }

  overall(col: any) {
    return (col.fromGrant || 0) - this.sum(col);
  }

  get sumOverall() {
    return this.cols.reduce((s: any, col: any) => {
      return s + this.overall(col);
    }, 0);
  }

}
