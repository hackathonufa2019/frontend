import { Component, Prop, Vue, Watch } from 'vue-property-decorator';


@Component
export default class CommonInfo extends Vue {
  @Prop() pageData: any;

  form: any = null;

  @Watch('pageData')
  defS(val: any) {
    this.form = { ...this.pageData };
  }

  save() {
    this.$emit('on-save', this.form);
  }
}
