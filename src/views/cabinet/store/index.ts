import { Module } from 'vuex';
import { IAppState } from '@/store';
import Rest from '@/rest';
import { Vue } from 'vue-property-decorator';

export const saveInfo = 'saveInfo';
const cabinetModule: Module<any, IAppState> = {
  namespaced: true,
  actions: {
    [ saveInfo ]: ({ state }, form: any) => {
      if (state.page === 1 && state.subPage === 1) {
        Rest.saveCommonPage(form).then((res) => {
          //
        });
      }
    },
    cabinetInit: ({ commit, rootState }, { page, subPage }) => {
      commit('setPages', { page, subPage });
      const userId = (rootState as any).auth.user.id;
      Rest.cabinetPage(userId, page, subPage).then(
        ({ data }: any) => {
          commit('setPageData', data);
        },
      );
    },
  },
  mutations: {
    setPageData: (state, data) => {
      Vue.set(state, 'pageData', data);
    },
    setPages(state, { page, subPage }) {
      state.page = page;
      state.subPage = subPage;
    },
  },
  state: {
    page: 1,
    subPage: 1,
    pageData: null,
  },
};

export default cabinetModule;
