import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';
import UserList from '@/views/admin/components/user-list/user-list';


const adminNs = namespace('admin');
@Component({
  components: {
    UserList,
  },
})
export default class Admin extends Vue {
  @adminNs.Getter('notUser') notUser: any;
  @adminNs.Getter('invalidUsers') invalidUsers: any;
  @adminNs.Getter('validUsers') validUsers: any;
}
