import { GetterTree, Module } from 'vuex';
import { IAppState } from '@/store';
import Rest from '@/rest';
import { Vue } from 'vue-property-decorator';

const adminModule: Module<any, IAppState> = {
  namespaced: true,

  actions: {
    init: ({ commit }) => {
      Rest.getAdminHome().then(({ data }) => {
        commit('setUsers', data);
      });
    },
  },
  mutations: {
    setUsers: (state, users) => {
      Vue.set(state, 'users', users);
    },
  },
  state: {
    users: [],
  },
  getters: {
    tUsers: (state) => {
      return state.users;
    },
    notUser: (state, getters) => {
      return (getters[ 'tUsers' ] || [] as any).filter((user: any) => user.status === -1);
    },
    invalidUsers: (state, getters) => {
      return (getters[ 'tUsers' ] || [] as any).filter((user: any) => user.status === 0);
    },
    validUsers: (state, getters) => {
      return (getters[ 'tUsers' ] || [] as any).filter((user: any) => user.status === 1);
    },
  },
};

export default adminModule;
