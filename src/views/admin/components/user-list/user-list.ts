import { Component, Prop, Vue } from 'vue-property-decorator';
import * as _ from 'lodash';

@Component
export default class UserList extends Vue {
  @Prop({ default: (): any[] => [] }) users: any[];
  expandedIds: any = {};


  userTitle(user: any) {
    return `${user.first_name} ${user.last_name}`;
  }

  expand(userId: string) {
    Vue.set(this.expandedIds, userId, !this.expandedIds[ userId ]);
  }

  userProps: any = [
    'last_name',
    'first_name',
    'middle_name',
    'inn',
    'documents.passport.document_data.series',
    'documents.passport.document_data.number',
    'documents.passport.document_data.vydan',
    'documents.passport.document_data.vydan_date',
    'documents.passport.document_data.address_reg',
    'documents.passport.document_data.address_fact',
  ];

  propName(prop: any): any {
    return ({
      'last_name': 'Фамилия',
      'first_name': 'Имя',
      'middle_name': 'Отчество',
      'inn': 'ИНН',
      'documents.passport.document_data.series': 'Серия',
      'documents.passport.document_data.number': 'Номер',
      'documents.passport.document_data.vydan': 'Выдан',
      'documents.passport.document_data.vydan_date': 'Дата выдачи',
      'documents.passport.document_data.address_reg': 'Адрес рег.',
      'documents.passport.document_data.address_fact': 'Адрес факт.',
    } as any) [ prop ] as any;
  }

  getProp(user: any, prop: any) {
    return _.get(user, prop);
  }
}