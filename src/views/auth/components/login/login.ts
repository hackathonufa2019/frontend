import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';
import { LOGIN } from '@/views/auth/store';

const authNs = namespace('auth');


export interface ILoginForm {
  email: string;
  password: string;
}


@Component
export default class Login extends Vue {
  @authNs.Action(LOGIN) login: any;

  form: ILoginForm | any = {};
  emailRules = [
    (v: string) => !!v || 'E-mail is required',
    (v: string) => /.+@.+/.test(v) || 'E-mail must be valid',
  ];

  valid: boolean = true;

  mounted() {
    this.form = {
      email: '',
      password: '',
    };

  }

  submit() {
    if (this.valid) {
      this.login(this.form).then((
        response: any,
      ) => {
        window.location.href = '/';
      }, (error: any) => {

      });
    }

  }
}
