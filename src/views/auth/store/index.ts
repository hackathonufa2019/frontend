import { Module } from 'vuex';
import { IAppState } from '@/store';
import { ILoginForm } from '@/views/auth/components/login/login';
import Rest from '@/rest';
import { Vue } from 'vue-property-decorator';


export interface IAuthState {
  token: string;
  user: any;
}

export const LOGIN = 'LOGIN';
export const REGISTER = 'REGISTER';
export const CREATE_RANDOM_USER = 'CREATE_RANDOM_USER';
export const CREATE_ADMIN_USER = 'CREATE_ADMIN_USER';
export const localStorageAuthToken = 'localStorageAuthToken';
export const AUTH_INIT = 'AUTH_INIT';

const authModule: Module<IAuthState, IAppState> = {
  namespaced: true,
  actions: {
    [ AUTH_INIT ]: ({ commit }) => {
      return new Promise((resolve, reject) => {
        Rest.userMe()
          .then(({ data }: any) => {
            commit('setUser', data);
            resolve(data);
          })
          .catch((error: any) => reject(error));

      });
    },
    [ LOGIN ]: ({ commit }, payload: ILoginForm) => {
      Rest.login(payload).then(({ data }) => {
        commit('setToken', data.token);
      });
    },
    [ CREATE_RANDOM_USER ]: ({ commit }) => {
      return new Promise((resolve, reject) => {
        Rest.createRandomUser().then(({ data }) => {
          commit('setToken', data.token);
          resolve();
        }, (error: any) => {
          reject(error);
        });

      });
    },
    [ CREATE_ADMIN_USER ]: ({ commit }) => {
      return new Promise((resolve, reject) => {
        Rest.createAdminUser().then(({ data }) => {
          commit('setToken', data.token);
          resolve();
        }, (error: any) => {
          reject(error);
        });

      });
    },
  },
  state: {
    token: null,
    user: null,
  },
  mutations: {
    setToken: (state, token: string) => {
      localStorage.setItem(localStorageAuthToken, token);
    },
    setUser: (state, user: any) => {
      Vue.set(state, 'user', user);
    },
  },
};

export default authModule;
