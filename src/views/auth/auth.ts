import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';
import { CREATE_RANDOM_USER } from '@/views/auth/store';

const authNs = namespace('auth');

@Component
export default class Auth extends Vue {
  @authNs.Action(CREATE_RANDOM_USER) createRandomUser: any;
}
