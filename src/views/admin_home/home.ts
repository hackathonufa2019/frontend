import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';
import { CREATE_ADMIN_USER } from '@/views/auth/store';


const authNs = namespace('auth');
@Component
export default class Home extends Vue {
  @authNs.Action(CREATE_ADMIN_USER) createAdminUser: any;

  createUser() {
    this.createAdminUser().then(() => window.location.href = '/admin');
  }
}
