import { Component, Prop, Vue } from 'vue-property-decorator';

export interface TabObject {
  id: number;
  title: string;
  selected: boolean;

}

@Component
export default class Tabs extends Vue {
  @Prop() title: string;
  @Prop({ default: (): any[] => [] }) tabs: TabObject[];

}
