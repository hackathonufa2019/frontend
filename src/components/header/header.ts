import { Component, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';

const authNs = namespace('auth');


@Component
export default class Header extends Vue {
  @authNs.State(state => state.user) user: any;

  get displayName() {
    if (this.user) {
      return `${this.user.first_name} ${this.user.last_name}`;
    } else {
      return null;
    }
  }
}
