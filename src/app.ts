import { Component, Vue } from 'vue-property-decorator';
import Header from '@/components/header/header';

@Component({
  components: {
    Header,
  },
})
export default class App extends Vue {
}
