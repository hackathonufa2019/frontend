import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { localStorageAuthToken } from '@/views/auth/store';

export class AuthInterceptor {
  static request(req: AxiosRequestConfig): AxiosRequestConfig {
    const tokenStorageKey = localStorageAuthToken;

    if (localStorage.getItem(tokenStorageKey)) {
      const token = localStorage.getItem(tokenStorageKey);

      req.headers.common.Authorization = 'JWT ' + token;
    }

    return req;
  }

  static successResponse(res: AxiosResponse): Promise<AxiosResponse<any>> {
    return Promise.resolve(res);
  }

  static errorResponse(value: AxiosError): Promise<AxiosResponse<any>> {
    return Promise.reject(value.response);
  }
}
