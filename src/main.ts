import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css'; // Ensure you are using css-loader
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import BootstrapVue from 'bootstrap-vue';
import VueAxios from 'vue-axios';
import { AxiosConfig } from '@/rest';
import { AuthInterceptor } from '@/interceptors';

Vue.use(BootstrapVue);


Vue.use(Vuetify);
Vue.use(VueAxios, axios);

Object.assign(Vue.axios.defaults, AxiosConfig);

Vue.axios.interceptors.request.use(AuthInterceptor.request);
Vue.axios.interceptors.response.use(AuthInterceptor.successResponse, AuthInterceptor.errorResponse);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
